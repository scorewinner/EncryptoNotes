package com.encryptonotes;

/**
 * Created by david on 25.09.17.
 */

public class Note {

    int _id;
    String _title;
    String _text;

    public Note(){

    }

    public Note(int id, String title, String text){
        this._id = id;
        this._title = title;
        this._text = text;

    }

    public Note(int id, String title) {
        this._id = id;
        this._title = title;
    }

    // getting ID
    public int getID(){
        return this._id;
    }

    // setting id
    public void setID(int id){
        this._id = id;
    }

    // getting title
    public String getTitle(){
        return this._title;
    }

    // setting title
    public void setTitle(String title){
        this._title = title;
    }

    // getting text
    public String getText(){
        return this._text;
    }

    // setting text
    public void setText(String text){
        this._text = text;
    }

}
