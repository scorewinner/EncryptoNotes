package com.encryptonotes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * Created by david on 25.09.17.
 */


public class AddNote extends Activity {

    String title;
    int content;

    public EditText editTextTitle;
    public EditText editTextContent;
    public Button saveBtn;

    DatabaseHandler db = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_page);

        editTextTitle = (EditText) findViewById(R.id.editTextTitle);
        editTextContent = (EditText) findViewById(R.id.editTextContent);
        saveBtn = (Button) findViewById(R.id.buttonNoteSave);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addNote();
                goBack();
                //save note and switch activity
            }
        });


        SensorManager mSensorManager;

        ShakeActivity mSensorListener;


        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorListener = new ShakeActivity();

        mSensorListener.setOnShakeListener(new ShakeActivity.OnShakeListener() {

            public void onShake() {
                Toast.makeText(getApplicationContext(), "Shake detected!", Toast.LENGTH_SHORT).show();
                goBack();

            }
        });


    }

    public void addNote(){
        String title = editTextTitle.getText().toString();
        String text = editTextContent.getText().toString();

        db.addNote(title, text);
    }
    public void updateNote(){

    }


    void goBack(){
        Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
        Intent backToMainIntent = new Intent(AddNote.this, MainPage.class);
        AddNote.this.startActivity(backToMainIntent);
    }

    public void openNote(){


        editTextTitle = (EditText) findViewById(R.id.editTextTitle);
        editTextContent = (EditText) findViewById(R.id.editTextContent);

        editTextTitle.setText(title);
        editTextContent.setText(content);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(int content) {
        this.content = content;
    }
}
