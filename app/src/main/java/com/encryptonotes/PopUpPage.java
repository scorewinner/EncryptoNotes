package com.encryptonotes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by admin on 25.09.2017.
 */
public class PopUpPage extends Activity {

    public EditText editText1;
    public EditText editText2;
    public Button save;

    DatabaseHandler db = new DatabaseHandler(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_layout);


        editText1 = (EditText) findViewById(R.id.editText2);
        editText2 = (EditText) findViewById(R.id.editText);
        save = (Button) findViewById(R.id.saveButton);

        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               checkPassword();
            }
        });

    }

    void checkPassword() {
        String t1 = editText1.getText().toString();
        String t2 = editText2.getText().toString();

        if (t1.equals(t2)) {
            db.updatePassword(t1);
            Intent backIntent = new Intent(PopUpPage.this, MainPage.class);
            PopUpPage.this.startActivity(backIntent);
        } else {
            Toast.makeText(getApplicationContext(), "Password don't match, please try again", Toast.LENGTH_LONG).show();
        }


    }
}
