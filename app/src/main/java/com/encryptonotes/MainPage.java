package com.encryptonotes;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import java.util.List;


public class MainPage extends AppCompatActivity {

    DatabaseHandler db = new DatabaseHandler(this);

    public MainPage() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        createTextView();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent noteIntent = new Intent(MainPage.this, AddNote.class);
                MainPage.this.startActivity(noteIntent);
            }
        });

        db.addPasswordIfNotExists("password", " ");

        if (db.getPassword(5).equals(" ")) {
            Intent myIntent = new Intent(MainPage.this, PopUpPage.class);
            MainPage.this.startActivity(myIntent);
        }

        db.deleteNote(1);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_theme) {
            return true;
        }
        if (id == R.id.action_password) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void createTextView() {

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

        List<Note> noteList = db.getAllNotes();

        for (final Note nt : noteList) {
            Button button = new Button(this);

            button.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            button.setText(nt.getTitle());
            button.setId(nt.getID());
            System.out.println(nt.getID());
            linearLayout.addView(button);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddNote addNote = new AddNote();
                    addNote.setTitle(nt.getTitle());
                    addNote.setContent(nt.getID());
                    //  addNote.openNote(nt.getID(), nt.getTitle());

                    //OpenNote on = new OpenNote(nt.getTitle());


                    Intent in = new Intent(MainPage.this,OpenNote.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("Key1", nt.getTitle());
                    bundle.putInt("Key2", nt.getID());
                    in = in.putExtras(bundle);

                    startActivity(in);

                    /*Intent openNoteIntent = new Intent(MainPage.this, OpenNote.class);
                    MainPage.this.startActivity(openNoteIntent);*/


                }

            });
        }

    }

}



