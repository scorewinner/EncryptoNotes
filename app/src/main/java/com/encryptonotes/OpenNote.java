package com.encryptonotes;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

/**
 * Created by david on 26.09.17.
 */

public class OpenNote extends Activity {


    public EditText editTextTitle;
    public EditText editTextContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_page);

        editTextTitle = (EditText) findViewById(R.id.editTextTitle);
        editTextContext = (EditText) findViewById(R.id.editTextContent);
        DatabaseHandler db = new DatabaseHandler(this);

        Bundle bundle = getIntent().getExtras();
        String valueReceived1 = bundle .getString("Key1");
        //String valueReceived2 = bundle .getString("Key2");

        setTitle(valueReceived1);
      //  String context = db.getNoteContext(Integer.parseInt(valueReceived2));
   //     setContext(context);
    }

    void setTitle(String title) {
        editTextTitle.setText(title);
    }
    void setContext(String context) {
        editTextContext.setText(context);
    }

}
