package com.encryptonotes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 25.09.2017.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "encryptoNotes";

    // Password table name
    private static final String TABLE_PASSWORD = "password";

    // Password Table Columns names
    private static final String KEY_PASSWORD_ID = "id";
    private static final String KEY_PASSWORD = "password";


    //Notes table name
    private static final String TABLE_NOTES = "notes";


    // Notes Table Columns names
    private static final String KEY_NOTE_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_TEXT = "text";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PASSWORD_TABLE = "CREATE TABLE " + TABLE_PASSWORD + "("
                + KEY_PASSWORD_ID + " INTEGER PRIMARY KEY," + KEY_PASSWORD + " TEXT"
                + ")";
        db.execSQL(CREATE_PASSWORD_TABLE);

        String CREATE_NOTES_TABLE = "CREATE TABLE " + TABLE_NOTES + "("
                + KEY_NOTE_ID + " INTEGER PRIMARY KEY," + KEY_TITLE + " TEXT,"
                + KEY_TEXT + " TEXT" + ")";
        db.execSQL(CREATE_NOTES_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PASSWORD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);

        // Create tables again
        onCreate(db);
    }

    // CRUD (Create, Read, Update, Delete) Operations


    // Add Password into SQLite
    void addPassword(String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PASSWORD, password); //Password

        db.insert(TABLE_PASSWORD, null, values);
        db.close();
    }

    // get Password
    String getPassword(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(TABLE_PASSWORD, new String[] { KEY_PASSWORD_ID, KEY_PASSWORD}, KEY_PASSWORD_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        String password = cursor.getString(1);

        return password;
    }

    // To add a password if none exists as a first thing when starting app, afterwards it wont do it
    void addPasswordIfNotExists(String tableName, String item) {

        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("INSERT INTO " + tableName + " (" + TABLE_PASSWORD + ") SELECT * FROM (SELECT '" + item  + "') WHERE NOT EXISTS ( SELECT " + TABLE_PASSWORD + " FROM " + tableName + " WHERE " + TABLE_PASSWORD + " = '" + item + "') LIMIT 1;");
    }

    // To update the first password which got set automatically
    void updatePassword(String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        String where = "id = 5" ;
        ContentValues cv = new ContentValues();
        cv.put("password", password);
        db.update("password", cv, where, null);
    }

    // To add note
    void addNote(String title, String text) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, title); //Add title
        values.put(KEY_TEXT, text); //Add text


        db.insert(TABLE_NOTES, null, values);
        db.close();


    }

    String getNoteContext(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NOTES, new String[] { KEY_NOTE_ID, KEY_TITLE, KEY_TEXT}, KEY_NOTE_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        String noteName = cursor.getString(2);

        return noteName;
    }

    // When the Note is saved (or the phone shaken later)
    void updateNote(String title, String text) {
        SQLiteDatabase db = this.getWritableDatabase();

        String where = "id = 5" ;
        ContentValues cv = new ContentValues();
        cv.put(KEY_TITLE, title);
        cv.put(KEY_TEXT, text);
        db.update(TABLE_NOTES, cv, where, null);

        Cursor cursor = db.rawQuery("select age,name from tblTest1", null);

        cursor.close();
    }


    // To show all notes in for loop in overview
    List<Note> getAllNotes() {
        List<Note> noteList = new ArrayList<Note>();
        // Select All Query
        String selectQuery = "SELECT  id, title FROM " + TABLE_NOTES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setID(Integer.parseInt(cursor.getString(0)));
                note.setTitle(cursor.getString(1));
                // Adding contact to list
                noteList.add(note);
            } while (cursor.moveToNext());
        }

        // return contact list
        return noteList;
    }

    // To delete a note when u click on that option
    void deleteNote(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTES, KEY_NOTE_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }

    public int countNote(){

        SQLiteDatabase db = this.getReadableDatabase();
        long cnt  = DatabaseUtils.queryNumEntries(db, TABLE_NOTES);
        db.close();

        return (int) cnt;
    }
    void changePassword(String oldPassword, String newPassword){

    }
}
